<?php include 'base/top.php'; ?>
<div id="message-container">
    <div id="message-one" class="message">
        <h2>We Drive</h2>
        <p>While we try our best to find finance solutions for everyone, the availability of specific vehicles and rates vary based on your location.</p>
        <p>Narrow your search by selecting the basic model of vehicle you are looking for.</p>
        <p>Select your maximum monthly budget and insert your postal code and we will look for a local program and suitable vehicles in your area.</p>
    </div>
    <div id="message-two" class="message">
        <h2>Congratulations!</h2>
        <h3> There are programs in <span id="area"></span></h3>
        <p>To continue your application for an auto loan we need to collect some basic information including your personal contact info, and details regarding your current housing and employment status.</p>
        <p>Once completed you will be able to submit your application.</p>
    </div>
    <div id="message-three" class="message">
        <h2>Almost done!</h2>
        <p> Once you finish this final form and submit it your application will be sent for review!</p>
        <p>Our extensive dealer network can often have your application accepted in hours. You will receive an email with your successful application details. You can also log in to your members area to access all of your financing information.</p>
    </div>
    <div id="success" class="message">
        <h2>Thank You!</h2>
        <p>Your application is being sent now. One of our approval specialists will contact you shortly.</p>
        <p>To accelerate the application process, please confirm the important information on the right.</p>
    </div>
    <div id="success2" class="message">
        <h2>Success</h2>
        <p>Your application has been sent.</p>
        <p>An approval specialist will be contacting you to complete the application process.</p>
        <p>Email any additional information or contact numbers to <a href="mailto:info@wedrivealberta.c">info@wedrivealberta.com</a>.</p>   
    </div>
    <div id="error" class="message">
        <h2>Oops!</h2>
        <p>The credit application was not successfully sent. Please try again later.</p>
    </div>
    <span id="message-close"></span>
</div>
<div class="container-fluid">
    <div class="row">     
        <div id="form-container">
            <div class="row">
                <div class="step-container">
                    <div class="step inprogress incomplete"><h3>Step 1</h3><span>Check Availability</span></div>
                    <div class="step incomplete"><h3>Step 2</h3><span>Personal Contact</span></div>
                    <div class="step incomplete"><h3>Step 3</h3><span>Housing &amp; Employment</span></div>
                </div>
            </div>
            <form id="credit-form" method="POST" name="form" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
                <fieldset class="step-one">
                    <input type="hidden" name="form_name" value="credit-application">
                    <input type="hidden" name="dealer_id" value="777">
                    <input type="hidden" name="lead_type" value="credit">
                    <input type="hidden" name="lead_source" value="wedrivealberta.com">
                    <input type="hidden" id="form_message" name="message">
                    <div class="form-group">
                        <label for="vehicle_selected">What kind of vehicle are you looking for?</label>
                        <input id="vehicle-select" type="hidden" name="vehicle_selected" value="Car">
                        <div class="vehicle-container">
                            <div class="row">
                                <div class="vehicle active" data-vehicle="Car">
                                    <img src="/crd-we-drive-alberta/static/img/car-white.png" alt="Car">
                                    <span>Car</span>
                                </div>
                                <div class="vehicle" data-vehicle="SUV">
                                    <img src="/crd-we-drive-alberta/static/img/suv-white.png" alt="SUV">
                                    <span>SUV</span>
                                </div>
                                <div class="vehicle" data-vehicle="Truck">
                                    <img src="/crd-we-drive-alberta/static/img/truck-white.png" alt="Truck">
                                    <span>Truck</span>
                                </div>
                                <div class="vehicle" data-vehicle="Van">
                                    <img src="/crd-we-drive-alberta/static/img/van-white.png" alt="Van">
                                    <span>Van</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="car_payment_budget">What is your monthly budget?</label>
                        <div class="slider-container">
                            <div class="slider-value-wrapper">
                                <h3>$<span class="slider-value">325</span></h3>
                            </div>
                            <div class="slider-wrapper">
                                <div class="slider"></div>
                            </div>
                            <input class="slider-input" type="hidden" name="car_payment_budget" id="car_payment_budget" value="325">
                        </div>
                    </div>
                    <div class="form-group ignore">
                        <label for="pcode">Enter your postal code to check availability</label>
                        <input id="pcode" name="pcode" type="text" value="T6V1G4" required>
                    </div>
                    <div class="pagination">
                        <span class="next">Next Step</span>
                    </div>
                </fieldset>
                <fieldset class="step-two">
                    <div class="group-heading">
                        <h3>Personal Info</h3>
                    </div>
                    <div class="form-group">
                        <label for="first_name">First name</label>
                        <input id="first_name" name="first_name" type="text" placeholder="John" value="Ryan" required>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last name</label>
                        <input id="last_name" name="last_name" type="text" placeholder="Doe" value="Quang" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" name="email" placeholder="you@example.com" value="rquang@gmail.com" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="primary_phone">Phone</label>
                        <input id="primary_phone" name="primary_phone" placeholder="(555)-555-5555" type="tel" value="7807083641" required>
                    </div>
                    <div class="form-group">
                        <label for="dob">Birthdate</label>
                        <div class="dob">
                            <div class="row">
                                <select id="dob-month" class="dob-month" name="dob_month" required>
                                    <option value="" selected>Month</option>
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March" selected>March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                </select>
                                <select name="dob_day" class="dob-day" id="dob_day" required>
                                    <option value="" selected>Day</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28" selected>28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                                <select id="dob_year" class="dob-year" name="dob_year" required>
                                    <option value="" selected>Year</option>
                                    <option value="1996">1996</option>
                                    <option value="1995">1995</option>
                                    <option value="1994">1994</option>
                                    <option value="1993">1993</option>
                                    <option value="1992">1992</option>
                                    <option value="1991" selected>1991</option>
                                    <option value="1990">1990</option>
                                    <option value="1989">1989</option>
                                    <option value="1988">1988</option>
                                    <option value="1987">1987</option>
                                    <option value="1986">1986</option>
                                    <option value="1985">1985</option>
                                    <option value="1984">1984</option>
                                    <option value="1983">1983</option>
                                    <option value="1982">1982</option>
                                    <option value="1981">1981</option>
                                    <option value="1980">1980</option>
                                    <option value="1979">1979</option>
                                    <option value="1978">1978</option>
                                    <option value="1977">1977</option>
                                    <option value="1976">1976</option>
                                    <option value="1975">1975</option>
                                    <option value="1974">1974</option>
                                    <option value="1973">1973</option>
                                    <option value="1972">1972</option>
                                    <option value="1971">1971</option>
                                    <option value="1970">1970</option>
                                    <option value="1969">1969</option>
                                    <option value="1968">1968</option>
                                    <option value="1967">1967</option>
                                    <option value="1966">1966</option>
                                    <option value="1965">1965</option>
                                    <option value="1964">1964</option>
                                    <option value="1963">1963</option>
                                    <option value="1962">1962</option>
                                    <option value="1961">1961</option>
                                    <option value="1960">1960</option>
                                    <option value="1959">1959</option>
                                    <option value="1958">1958</option>
                                    <option value="1957">1957</option>
                                    <option value="1956">1956</option>
                                    <option value="1955">1955</option>
                                    <option value="1954">1954</option>
                                    <option value="1953">1953</option>
                                    <option value="1952">1952</option>
                                    <option value="1951">1951</option>
                                    <option value="1950">1950</option>
                                    <option value="1949">1949</option>
                                    <option value="1948">1948</option>
                                    <option value="1947">1947</option>
                                    <option value="1946">1946</option>
                                    <option value="1945">1945</option>
                                    <option value="1944">1944</option>
                                    <option value="1943">1943</option>
                                    <option value="1942">1942</option>
                                    <option value="1941">1941</option>
                                    <option value="1940">1940</option>
                                    <option value="1939">1939</option>
                                    <option value="1938">1938</option>
                                    <option value="1937">1937</option>
                                    <option value="1936">1936</option>
                                    <option value="1935">1935</option>
                                    <option value="1934">1934</option>
                                    <option value="1933">1933</option>
                                    <option value="1932">1932</option>
                                    <option value="1931">1931</option>
                                    <option value="1930">1930</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="group-heading">
                        <h3>Address</h3>
                    </div>
                    <div class="form-group">
                        <label for="street_address_1">Address</label>
                        <input id="street_address_1" name="street_address_1" type="text" value="13344-154 A Avenue" required>
                    </div>
                    <div class="form-group">
                        <label for="city">City</label>
                        <input id="city" name="city" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="province">Province</label>
                        <input id="province" name="province" type="text" required>
                    </div>
                    <div class="form-group">
                        <label for="postal_code">Postal Code</label>
                        <input id="postal_code" name="postal_code" type="text" required>
                    </div>
                    <div class="pagination">
                        <span class="prev">Prev Step</span>
                        <span class="next">Next Step</span>
                    </div>
                </fieldset>
                <fieldset class="step-three">
                    <div class="group-heading">
                        <h3>Housing Info <span>(based on current residence)</span></h3>
                    </div>
                    <div class="form-group form-question">
                        <label for="rent_own">Do you rent or own?</label>
                        <div class="form-answer active" data-answer="rent">
                            <span>Rent</span>
                        </div>
                        <div class="form-answer" data-answer="own">
                            <span>Own</span>
                        </div>
                        <input type="hidden" name="rent_own" id="rent_own" value="Rent">
                    </div>
                    <div class="form-group">
                        <label for="house_duration">Duration</label>
                        <div class="house-duration">
                            <div class="row">
                                <select id="house_years" class="house-years" name="house_years" required>
                                    <option value="" selected>Years</option>
                                    <option value="0 Years">0 Years</option>
                                    <option value="1 Year">1 Year</option>
                                    <option value="2 Years">2 Years</option>
                                    <option value="3 Years">3 Years</option>
                                    <option value="4 Years">4 Years</option>
                                    <option value="5 Years">5 Years</option>
                                    <option value="6 Years">6 Years</option>
                                    <option value="7 Years">7 Years</option>
                                    <option value="8 Years">8 Years</option>
                                    <option value="9 Years" selected>9 Years</option>
                                    <option value="10 Years">10 Years</option>
                                    <option value="11 Years">11 Years</option>
                                    <option value="12 Years">12 Years</option>
                                    <option value="13 Years">13 Years</option>
                                    <option value="14 Years">14 Years</option>
                                    <option value="15 Years">15 Years</option>
                                    <option value="16 Years">16 Years</option>
                                    <option value="17 Years">17 Years</option>
                                    <option value="18 Years">18 Years</option>
                                    <option value="19 Years">19 Years</option>
                                    <option value="20 Years">20 Years</option>
                                    <option value="21 Years">21 Years</option>
                                    <option value="22 Years">22 Years</option>
                                    <option value="23 Years">23 Years</option>
                                    <option value="24 Years">24 Years</option>
                                    <option value="25 Years">25 Years</option>
                                    <option value="25 + Years">25+ Years</option>
                                </select>
                                <select id="house_months" class="house-months" name="house_months" required>
                                    <option value="" selected>Months</option>
                                    <option value="0 Months">0 Months</option>
                                    <option value="1 Month">1 Month</option>
                                    <option value="2 Months">2 Months</option>
                                    <option value="3 Months">3 Months</option>
                                    <option value="4 Months">4 Months</option>
                                    <option value="5 Months">5 Months</option>
                                    <option value="6 Months">6 Months</option>
                                    <option value="7 Months">7 Months</option>
                                    <option value="8 Months">8 Months</option>
                                    <option value="9 Months">9 Months</option>
                                    <option value="10 Months">10 Months</option>
                                    <option value="11 Months" selected>11 Months</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="house_payment">House/Rent Payment</label>
                        <input type="text" name="house_payment" id="rent_payment" placeholder="Enter monthly payment" value="250" required>
                    </div>
                    <div class="group-heading">
                        <h3>Employment Info</h3>
                    </div>
                    <div class="form-group">
                        <label for="company_name">Company</label>
                        <input type="text" name="company_name" id="company_name" placeholder="Enter company name" value="Strathcom" required>
                    </div>
                    <div class="form-group">
                        <label for="job_title">Position</label>
                        <input type="text" name="job_title" id="job_title" placeholder="Enter job title" value="Web Developer" required>
                    </div>
                    <div class="form-group">
                        <label for="work_phone">Work Phone</label>
                        <input type="tel" name="work_phone" id="work_phone" placeholder="(555)-555-5555" value="7804563641" required>
                    </div>
                    <div class="form-group">
                        <label for="job_duration">Duration</label>
                        <div class="job-duration">
                            <div class="row">
                                <select id="job_years" class="job-years" name="job_years" class="special-placement" required>
                                    <option value="" selected>Years</option>
                                    <option value="0 Years" selected>0 Years</option>
                                    <option value="1 Year">1 Year</option>
                                    <option value="2 Years">2 Years</option>
                                    <option value="3 Years">3 Years</option>
                                    <option value="4 Years">4 Years</option>
                                    <option value="5 Years">5 Years</option>
                                    <option value="6 Years">6 Years</option>
                                    <option value="7 Years">7 Years</option>
                                    <option value="8 Years">8 Years</option>
                                    <option value="9 Years">9 Years</option>
                                    <option value="10 Years">10 Years</option>
                                    <option value="11 Years">11 Years</option>
                                    <option value="12 Years">12 Years</option>
                                    <option value="13 Years">13 Years</option>
                                    <option value="14 Years">14 Years</option>
                                    <option value="15 Years">15 Years</option>
                                    <option value="16 Years">16 Years</option>
                                    <option value="17 Years">17 Years</option>
                                    <option value="18 Years">18 Years</option>
                                    <option value="19 Years">19 Years</option>
                                    <option value="20 Years">20 Years</option>
                                    <option value="21 Years">21 Years</option>
                                    <option value="22 Years">22 Years</option>
                                    <option value="23 Years">23 Years</option>
                                    <option value="24 Years">24 Years</option>
                                    <option value="25 Years">25 Years</option>
                                    <option value="25 + Years">25+ Years</option>
                                </select>
                                <select id="job_months" class="job-months" class="special-placement" name="job_months" required>
                                    <option value="" selected>Months</option>
                                    <option value="0 Months">0 Months</option>
                                    <option value="1 Month">1 Month</option>
                                    <option value="2 Months">2 Months</option>
                                    <option value="3 Months">3 Months</option>
                                    <option value="4 Months">4 Months</option>
                                    <option value="5 Months">5 Months</option>
                                    <option value="6 Months">6 Months</option>
                                    <option value="7 Months">7 Months</option>
                                    <option value="8 Months">8 Months</option>
                                    <option value="9 Months" selected>9 Months</option>
                                    <option value="10 Months">10 Months</option>
                                    <option value="11 Months">11 Months</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="monthly_income">Monthly income</label>
                        <input type="text" name="monthly_income" id="monthly_income" placeholder="Enter monthly income" value="5000" required>
                    </div>
                    <div class="pagination">
                        <span class="prev">Prev Step</span>
                        <div class="form-button">
                            <button type="submit" name="form1-submit">Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <form id="additional-form" method="POST" name="form" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
                <fieldset>
                    <div class="form-group">
                        <label>Please Confirm Your Email</label>
                        <input id="email" name="email" placeholder="you@example.com" value="rquang@gmail.com" type="text" required>
                    </div>
                    <div class="group-heading"><h3>Please Confirm Your Income</h3></div>
                    <div class="form-group">
                        <label for="income_confirm">How Do You Get Paid?</label>
                        <select id="income_confirm" name="income_confirm">
                            <option value="Hourly">Hourly</option>
                            <option value="Weekly">Weekly</option>
                            <option value="BiWeekly">Bi-Weekly</option>
                            <option value="Twice a Month" selected>Twice a Month</option>
                            <option value="Monthly">Monthly</option>
                            <option value="Annual Salary">Annual Salary</option>
                        </select>
                    </div>
                    <div class="group-heading">
                        <h3>Are You Interested In Building Your Credit?</h3>
                    </div>
                    <div class="form-group form-question">
                        <label>If you select "Yes" we will email you more information when you click submit.</label>
                        <div class="form-answer active" data-answer="yes">
                            <span>Yes</span>
                        </div>
                        <div class="form-answer" data-answer="no">
                            <span>no</span>
                        </div>
                        <input type="hidden" name="Q1" id="Q1" value="yes">
                    </div>
                    <div class="form-group form-question">
                        <label>Subscribe me to the We Drive Canada newsletter so I can receive credit building tips and special offers.</label>
                        <div class="form-answer active" data-answer="yes">
                            <span>Yes</span>
                        </div>
                        <div class="form-answer" data-answer="no">
                            <span>no</span>
                        </div>
                        <input type="hidden" name="Q2" id="Q2" value="yes">
                    </div>
                    <div class="form-group form-question">
                        <label>I consent to receive other electronic communications, including exclusive offers, from We Drive Canada third party partners regarding products and services which may be of interest to me.</label>
                        <div class="form-answer active" data-answer="yes">
                            <span>Yes</span>
                        </div>
                        <div class="form-answer" data-answer="no">
                            <span>no</span>
                        </div>
                        <input type="hidden" name="Q3" id="Q3" value="yes">
                    </div>
                    <div class="pagination">
                        <div class="form-button">
                            <button type="submit" name="form2-submit">Submit</button>
                        </div>
                    </div>
                </fieldset>
            </form>
            <div id="car-load">
                <h2>Checking Availability</h2>
                <div class="car-load-wrapper">
                    <div class="car"></div>
                    <div class="progress-bar"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'base/bottom.php'; ?>