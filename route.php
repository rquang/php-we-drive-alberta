<?php

function post_to_url($url, $data) {
   $fields = '';
   foreach($data as $key => $value) { 
      $fields .= $key . '=' . $value . '&'; 
   }
   rtrim($fields, '&');

   $post = curl_init();

   curl_setopt($post, CURLOPT_URL, $url);
   curl_setopt($post, CURLOPT_POST, count($data));
   curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
   curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);

   $result = curl_exec($post);

   curl_close($post);
}

function mail_to($formcontent, $email, $subject) {
    $recipient = "rquang@strathcom.com";
    $mailheader = "From: $email \r\n";
    mail($recipient, $subject, $formcontent, $mailheader) or die("Error!");  
}

if (isset($_POST['form1-submit'])) {
    $lead_type = $_POST['lead_type'];
    $lead_source = $_POST['lead_source'];
    $form_name = $_POST['form_name'];
    $dealer_id = $_POST['dealer_id'];
    $message = $_POST['message'];
    
    $vehicle = $_POST['vehicle_selected'];
    $car_budget = $_POST['car_payment_budget'];
    
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $phone = $_POST['primary_phone'];
    $email = $_POST['email'];
    $dob_month = $_POST['dob_month'];
    $dob_day = $_POST['dob_day'];
    $dob_year = $_POST['day_year'];
    
    $address = $_POST['address'];
    $city = $_POST['city'];
    $prov = $_POST['province'];
    $pcode = $_POST['postal_code'];
    
    $rent_own = $_POST['rent_own'];
    $house_years = $_POST['house_years'];
    $house_months = $_POST['house_months'];
    $house_payment = $_POST['house_payment'];
    
    $company_name = $_POST['company_name'];
    $position = $_POST['job_title'];
    $work_phone = $_POST['work_phone'];
    $job_years = $_POST['job_years'];
    $job_months = $_POST['job_months'];
    $monthly_income = $_POST['monthly_income'];
    
    $data = array(
      "lead_type" => $lead_type,
      "lead_source" => $lead_source,
      "form_name" => $form_name,
      "dealer_id" => $dealer_id,
      "first_name" => $first_name,
      "last_name" => $last_name,
      "email" => $email,
      "primary_phone" => $phone,
      "street_address_1" => $address,
      "city" => $city,
      "province" => $prov,
      "postal_code" => $pcode,
      "message" => $message);

      post_to_url("http://leads.strathcom.com/api/credit",$data);

    
    $formcontent = "
  CAR INFO 
  Car Type: $vehicle
  Car Monthly Budget: $car_budget 

  PERSONAL INFO 
  Name: $first_name $last_name
  Email: $email 
  Phone: $phone 

  ADDRESS INFO 
  Address: $address
  City: $city
  Province: $prov
  Postal Code: $pcode

  HOUSING INFO
  Rent/Own: $rent_own
  House Occupancy: $house_years $house_months
  Monthy Payment: $house_payment

  EMPLOYMENT INFO
  Company Name: $company_name
  Position: $position
  Work Phone: $work_phone
  Job Duration: $job_years $job_months
  Monthly Income: $monthly_income

  - You received this message because $first_name filled out the credit application form at www.wedrivealberta.com -
  ";
  
  mail_to($formcontent, $email, "We Drive Alberta - Credit Application");
}

if (isset($_POST['form2-submit'])) {
    $email = $_POST['email'];
    $Q1 = $_POST['Q1'];
    $Q2 = $_POST['Q2'];
    $Q3 = $_POST['Q3'];
    $income_confirm = $_POST['income_confirm'];

    $formcontent = "
  Confirmation Email: $email 

  ADDITIONAL CREDIT INFO 
  How do you get paid? $income_confirm

  ARE YOU INTERESTED IN BUILDING CREDIT?
  Email more information? $Q1
  Subscribe to Newsletter: $Q2 
  Receive other electronic communication? $Q3

  - You received this message because the additional credit info form was filled out -
  ";

  mail_to($formcontent, $email, "We Drive Alberta - Credit Application Continued");
}

?>