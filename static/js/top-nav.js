$(function() {
    function closeNav() {
        if ($('.top-nav').hasClass('active')) {
            $('.top-nav-collapse').replaceWith(cloneObj);
        }
        $(".top-nav").toggleClass("active");
    }
    function loadNav() {
        $('body').css("padding-top", $(".top-nav-header").outerHeight());
        
        $(".top-nav-toggle").on("click", closeNav);

        $('.top-nav-inner li').on('click', function() {
            if ($(this).hasClass('collapsed')) {
                if ($(this).find("> ul").length) {
                    $(this).siblings('.uncollapsed').find("> ul").slideToggle("fast");
                    $(this).siblings('.uncollapsed').toggleClass('collapsed uncollapsed');
                    $(this).toggleClass('collapsed uncollapsed');
                    $(this).find("> ul").slideToggle("fast");
                    return false;
                }
            }
        });
    }

    var cloneObj;

    if ($(window).width() < 992) {
        cloneObj = $('.top-nav-collapse').clone();
        loadNav();
    }
    $(window).resize(function() {
        if ($(window).width() > 992) {
            $('body').css("padding-top", 0);
             $('.top-nav-inner li').unbind('click');
        } else {
            $('body').css("padding-top", $(".top-nav-header").outerHeight());
            cloneObj = $('.top-nav-collapse').clone();
            loadNav();
        }
    });
});