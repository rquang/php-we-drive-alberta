var opts = {
    color: '#fff'
};
var target = document.getElementById('loader');
var spinner = new Spinner(opts).spin(target);
var form = $("#credit-form");
var form2 = $("#additional-form");

$(document).ready(function() {
    jQuery.validator.addMethod("cdnPostal", function(postal, element) {
        return this.optional(element) ||
            postal.match(/[a-zA-Z][0-9][a-zA-Z](-| |)[0-9][a-zA-Z][0-9]/);
    }, "Please specify a valid postal code.");

    form.validate({
        debug: true,
        rules: {
            postal_code: {
                required: true,
                cdnPostal: true
            },
            firstname: "required",
            lastname: "required",
            email: {
                required: true,
                email: true
            },
            primary_phone: {
                required: true,
                phoneUS: true
            },
            street_address_1: {
                required: true
            },
            city: {
                required: true
            },
            province: {
                required: true
            },
            pcode: {
                required: true,
                cdnPostal: true
            }
        },
        groups: {
            DOB: "dob_day dob_month dob_year",
            Address_time: "house_years house_months",
            Job_time: "job_years job_months"
        },
        errorElement: "p",
        submitHandler: function() {
            var consolidatedMessage = "";
            $("#credit-form .form-group").not(".ignore").each(function() {
                var b = "";
                if ($(this).find("input").length) {
                    b = $(this).find("input").val();
                } else {
                    $(this).find("select").each(function() {
                        b += $(this).val() + " ";
                    });
                }
                consolidatedMessage += $(this).find("label").attr("for") + ": " + b + "\n";
            });
            $("#form_message").val(consolidatedMessage);
            $('.message').fadeOut();
            $.ajax({
                type: 'POST',
                dataType: 'text',
                crossDomain: true,
                url: '/crd-we-drive-alberta/route.php',
                data: $('#credit-form').serialize(),
                success: function(data) {
                    $('#success').fadeIn();
                    $('.step-container').fadeOut();
                    $("#credit-form").fadeOut(function() {
                        $("#additional-form").fadeIn();
                    });
                }
            });
        }
    });
    form2.validate({
        debug: true,
        rules: {
            email: {
                required: true,
                email: true
            }
        },
        errorElement: "p",
        submitHandler: function() {
            $('.message').fadeOut();
            $.ajax({
                type: 'POST',
                dataType: 'text',
                crossDomain: true,
                url: '/crd-we-drive-alberta/route.php',
                data: $('#additional-form').serialize(),
                success: function(data) {
                    $('#success2').fadeIn();
                    $('#form-container').hide("slide", {
                        direction: "right"
                    }, 600);
                }
            });
        }
    });
});

$(window).load(function() {
    function backTop() {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 200) {
                $('.back-to-top').addClass('show');
            } else {
                $('.back-to-top').removeClass('show');
            }
        });
        // Animate the scroll to top
        $('.back-to-top').click(function(event) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, 300);
        });
    }

    // gets location based on postal code added
    function getLocation() {
        var city, prov;

        $.getJSON("http://www.geonames.org/postalCodeLookupJSON?&country=CA&callback=?", {
            postalcode: $("#pcode").val().replace(/\s/g, '')
        }, function(response) {
            if (response && response.postalcodes.length && response.postalcodes[0].placeName) {
                city = response.postalcodes[0].placeName.split('(');
                prov = response.postalcodes[0].adminName1.split('(');
                $("#city").val(city[0].trim());
                $("#area").html(city[0].trim());
                $("#province").val(prov[0].trim());
                $("#postal_code").val($("#pcode").val());
            }
        });
    }

    // cycles back and forth through the form by pagination   
    function pageStep() {
        var current = $(this),
            currentStep = current.closest('fieldset');

        function nextStep() {
            currentStep.fadeOut(function() {
                $("#message-container").fadeOut(function() {
                    $(".message").fadeOut();
                    currentStep.next().fadeIn(function() {
                        $(".message:eq(" + (currentStep.index() + 1) + ")").fadeIn(function() {
                            $("#message-container").fadeIn();
                        });
                    });
                });
                $(".step:eq(" + currentStep.index() + ")").removeClass("inprogress incomplete").addClass("complete");
                $(".step:eq(" + (currentStep.index() + 1) + ")").addClass("inprogress");
            });
        }

        function prevStep() {
            currentStep.fadeOut(function() {
                currentStep.prev().fadeIn();
                if (!$(".step:eq(" + (currentStep.index() - 1) + ")").hasClass("complete")) {
                    $(".step:eq(" + (currentStep.index() - 1) + ")").addClass("inprogress");
                }
            });
        }

        if (current.hasClass("next")) {
            if (form.valid()) {
                if (!currentStep.index()) {
                    getLocation();
                    if (!current.hasClass('loaded')) {
                        carLoader(nextStep);
                        current.addClass('loaded');
                    } else {
                        nextStep();
                    }
                    // getLocation();
                    // nextStep();
                } else {
                    nextStep();
                }
            } else {
                $(".step:eq(" + (currentStep.index()) + ")").removeClass("complete").addClass("incomplete");
            }
        } else {
            prevStep();
        }
    }

    // progression bar loader
    function carLoader(callback) {
        $('html, body').animate({
            scrollTop: 0
        }, 300);
        var loader = $("#car-load");
        form.fadeOut('fast', function() {
            loader.fadeIn(function() {
                loader.find(".car").animate({
                    "left": "13%",
                }, 1200).animate({
                    "left": "20%",
                }, 600).animate({
                    "left": "50%",
                }, 2000).animate({
                    "left": "53%",
                }, 400).animate({
                    "left": "75%",
                }, 1000).animate({
                    "left": "80%",
                }, 300).animate({
                    "left": "100%",
                    "margin-left": "-70px"
                }, 600).delay(6300, function() {
                    $("#credit-form fieldset").hide();
                    loader.fadeOut(function() {
                        $("#area").html($("#city").val());
                        form.fadeIn(callback());
                    });
                });
            });
        });
    }

    $('#loader').fadeOut(function() {
        $('#wrapper').addClass('loaded');
        $('#form-container').animate({
            width: 'toggle'
        }, 500, function() {
            $(this).find(">.row, #credit-form").fadeIn();
        });
    });

    backTop();

    // vehicle selector
    $(".vehicle").on("click", function() {
        $("#vehicle-select").val($(this).data("vehicle"));
        $(".vehicle").removeClass("active");
        $(this).addClass("active");
    });

    // closes message box on mobile
    $("#message-close").on("click", function() {
        $("#message-container").fadeOut();
    });

    // form question selector
    $(".form-question >div").on("click", function() {
        $(this).siblings("input").val($(this).data("answer"));
        $(this).siblings(".form-answer").removeClass("active");
        $(this).addClass("active");
    });

    // initializes slider
    $(".slider").slider({
        range: "max",
        min: 150,
        max: 995,
        value: 325,
        slide: function(event, ui) {
            $(".slider-value").html(ui.value);
        }
    });

    // disable form submit on enter
    form.on("keyup keypress", function(e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            e.preventDefault();
            $(".pagination .next:visible").trigger('click', pageStep);
            return false;
        }
    });

    // binds pageStep event 
    $(".pagination span").on("click", pageStep);

    // cycles back and forth through form by step
    $(".step").on("click", function() {
        if ($(this).hasClass("inprogress") || (this).hasClass("complete")) {
            $("fieldset").hide();
            $("fieldset:eq(" + $(this).index() + ")").show();
        }
    });
});